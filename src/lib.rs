use std::{
    cell::RefCell,
    collections::{HashMap, HashSet, VecDeque},
    future::Future,
    panic::AssertUnwindSafe,
    pin::Pin,
    rc::Rc,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
    task::{Context, Poll, Wake, Waker},
    thread::{Thread, ThreadId},
};

use self::task_id::{TaskId, TaskIdGenerator};

thread_local! {
    static LOCAL_EXECUTOR: RefCell<Option<Rc<ExecutorState>>> = RefCell::new(None);
}

pub fn spawn<F: Future + 'static>(future: F) -> JoinHandle<F::Output> {
    with_local_exec(|exec| exec.spawn(future))
}

pub fn block_on<F: Future>(future: F) -> F::Output {
    Executor::new().block_on(future)
}

pub fn run_with<F: Future>(future: F) -> impl Future<Output = F::Output> + Unpin {
    Executor::new().run_with(future)
}

pub fn run_with_cooperative<F: Future>(future: F) -> impl Future<Output = F::Output> + Unpin {
    Executor::new().run_with_cooperative(future)
}

fn with_local_exec<T>(f: impl FnOnce(&Rc<ExecutorState>) -> T) -> T {
    LOCAL_EXECUTOR.with(|exec| {
        (f)(exec
            .borrow()
            .as_ref()
            .expect("Must be called from within an executor context"))
    })
}

mod task_id {
    use std::sync::atomic::AtomicUsize;

    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub(super) struct TaskId(usize);

    pub(super) struct TaskIdGenerator(AtomicUsize);

    impl TaskIdGenerator {
        pub(super) fn new() -> Self {
            TaskIdGenerator(AtomicUsize::new(0))
        }

        pub(super) fn generate(&self) -> TaskId {
            TaskId(self.0.fetch_add(1, std::sync::atomic::Ordering::Relaxed))
        }
    }

    impl Default for TaskIdGenerator {
        fn default() -> Self {
            Self::new()
        }
    }

    impl std::fmt::Display for TaskId {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "TaskId:{}", self.0)
        }
    }
}

fn joiner<T>() -> (JoinNotifier<T>, Rc<RefCell<JoinState>>, JoinHandle<T>) {
    let task_state = Rc::new(RefCell::new(TaskState {
        handle_waker: None,
        item: None,
        dropped: false,
    }));

    let join_state = Rc::new(RefCell::new(JoinState {
        task_waker: None,
        aborted: false,
    }));

    (
        JoinNotifier {
            task_state: Rc::clone(&task_state),
        },
        Rc::clone(&join_state),
        JoinHandle {
            task_state,
            join_state,
        },
    )
}

struct Executor {
    state: Rc<ExecutorState>,
}

struct ExecutorState {
    id_gen: TaskIdGenerator,
    remote_state: Arc<RemoteState>,
    woken: RefCell<VecDeque<TaskId>>,
    tasks: RefCell<HashMap<TaskId, Task>>,
    waker: RefCell<Option<Waker>>,
}

struct ExecutorToken;

struct RemoteState {
    woken: Mutex<VecDeque<TaskId>>,
    thread_id: ThreadId,
}

struct Task {
    future: Pin<Box<dyn Future<Output = ()>>>,
    join_state: Rc<RefCell<JoinState>>,
}

struct TaskState<T> {
    handle_waker: Option<Waker>,
    item: Option<T>,
    dropped: bool,
}

struct JoinState {
    task_waker: Option<Waker>,
    aborted: bool,
}

struct JoinNotifier<T> {
    task_state: Rc<RefCell<TaskState<T>>>,
}

pub struct JoinHandle<T> {
    join_state: Rc<RefCell<JoinState>>,
    task_state: Rc<RefCell<TaskState<T>>>,
}

#[derive(Debug)]
pub struct JoinError;

struct Drive<F> {
    future: Pin<Box<F>>,
    woken: VecDeque<TaskId>,
    executor: Executor,
    token: Option<ExecutorToken>,
    cooperative: bool,
}

struct ThreadWaker {
    thread: Thread,
}

struct MaybeLocalWaker {
    task_id: TaskId,
    remote_state: Arc<RemoteState>,
    parent: Waker,
}

struct MainWaker {
    woken: AtomicBool,
    parent: Waker,
}

impl Executor {
    fn new() -> Self {
        Self {
            state: Rc::new(ExecutorState::new()),
        }
    }

    fn block_on<F: Future>(self, future: F) -> F::Output {
        let parent = Arc::new(ThreadWaker::new()).into();
        let mut context = Context::from_waker(&parent);

        let mut drive = Drive::new(future, self, false);

        loop {
            if let Poll::Ready(res) = Pin::new(&mut drive).poll(&mut context) {
                return res;
            }
            std::thread::park();
        }
    }

    fn run_with<F: Future>(self, future: F) -> impl Future<Output = F::Output> + Unpin {
        Drive::new(future, self, false)
    }

    fn run_with_cooperative<F: Future>(self, future: F) -> impl Future<Output = F::Output> + Unpin {
        Drive::new(future, self, true)
    }
}

impl ExecutorState {
    fn new() -> Self {
        ExecutorState {
            id_gen: TaskIdGenerator::new(),
            remote_state: Arc::new(RemoteState::new()),
            woken: RefCell::new(VecDeque::new()),
            tasks: RefCell::new(HashMap::new()),
            waker: RefCell::new(None),
        }
    }

    fn spawn<F: Future + 'static>(&self, future: F) -> JoinHandle<F::Output> {
        let id = self.id_gen.generate();
        let (notifier, join_state, handle) = joiner();

        let task = Task {
            future: Box::pin(async move {
                notifier.ready(future.await);
            }),
            join_state,
        };

        self.tasks.borrow_mut().insert(id, task);
        self.woken.borrow_mut().push_back(id);
        if let Some(waker) = self.waker.borrow().as_ref() {
            waker.wake_by_ref();
        }

        handle
    }

    fn install(self: &Rc<Self>) -> ExecutorToken {
        if LOCAL_EXECUTOR
            .with(|exec| exec.borrow_mut().replace(Rc::clone(self)))
            .is_some()
        {
            panic!("Cannot create executor when executor already exists");
        }

        ExecutorToken
    }

    fn take_woken(&self) -> VecDeque<TaskId> {
        let mut woken = self.woken.borrow_mut().split_off(0);
        let mut remote_woken = self.remote_state.woken.lock().unwrap().split_off(0);

        let (out, _) = woken.drain(..).chain(remote_woken.drain(..)).fold(
            (VecDeque::new(), HashSet::new()),
            |(mut out, mut set), item| {
                if !set.contains(&item) {
                    out.push_back(item);
                    set.insert(item);
                }
                (out, set)
            },
        );

        out
    }
}

impl RemoteState {
    fn new() -> Self {
        RemoteState {
            woken: Mutex::new(VecDeque::new()),
            thread_id: std::thread::current().id(),
        }
    }
}

impl<T> JoinNotifier<T> {
    fn ready(self, item: T) {
        let mut guard = self.task_state.borrow_mut();
        guard.item = Some(item);
    }
}

impl<F> Drive<F>
where
    F: Future,
{
    fn new(future: F, executor: Executor, cooperative: bool) -> Self {
        Drive {
            future: Box::pin(future),
            woken: VecDeque::new(),
            executor,
            token: None,
            cooperative,
        }
    }

    fn poll_main_fut(
        &mut self,
        main_waker: &MainWaker,
        main_context: &mut Context<'_>,
    ) -> Poll<F::Output> {
        if main_waker.woken.swap(false, Ordering::AcqRel) {
            return self.future.as_mut().poll(main_context);
        }

        Poll::Pending
    }

    fn pop_next_task(&mut self) -> Option<(TaskId, Task)> {
        let mut tasks_guard = self.executor.state.tasks.borrow_mut();

        while let Some(task_id) = self.woken.pop_front() {
            if let Some(task) = tasks_guard.remove(&task_id) {
                return Some((task_id, task));
            }
        }

        None
    }
}

impl ThreadWaker {
    fn new() -> Self {
        ThreadWaker {
            thread: std::thread::current(),
        }
    }
}

impl<T> JoinHandle<T> {
    pub fn abort(&self) {
        let mut guard = self.join_state.borrow_mut();

        guard.aborted = true;

        if let Some(waker) = guard.task_waker.take() {
            waker.wake();
        }
    }
}

impl<T> Future for JoinHandle<T> {
    type Output = Result<T, JoinError>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut guard = self.task_state.borrow_mut();

        if let Some(item) = guard.item.take() {
            return Poll::Ready(Ok(item));
        }

        if guard.dropped {
            return Poll::Ready(Err(JoinError));
        }

        guard.handle_waker = Some(cx.waker().clone());
        drop(guard);

        Poll::Pending
    }
}

impl<F> Future for Drive<F>
where
    F: Future,
{
    type Output = F::Output;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        if this.token.is_none() {
            this.token = Some(this.executor.state.install());
        }

        let main_waker = Arc::new(MainWaker {
            woken: AtomicBool::new(true),
            parent: cx.waker().clone(),
        });

        let main_context = Arc::clone(&main_waker).into();
        let mut main_context = Context::from_waker(&main_context);

        if let Poll::Ready(res) = this.poll_main_fut(&main_waker, &mut main_context) {
            return Poll::Ready(res);
        }

        loop {
            let (task_id, mut task) = if let Some(tup) = this.pop_next_task() {
                tup
            } else {
                this.woken = this.executor.state.take_woken();

                if this.woken.is_empty() {
                    *this.executor.state.waker.borrow_mut() = Some(cx.waker().clone());
                    return Poll::Pending;
                }

                continue;
            };

            let guard = task.join_state.borrow();
            if guard.aborted {
                continue;
            }
            drop(guard);

            let maybe_local_waker = Arc::new(MaybeLocalWaker {
                task_id,
                remote_state: Arc::clone(&this.executor.state.remote_state),
                parent: cx.waker().clone(),
            })
            .into();
            let mut maybe_local_context = Context::from_waker(&maybe_local_waker);

            let res = std::panic::catch_unwind(AssertUnwindSafe(|| {
                task.future.as_mut().poll(&mut maybe_local_context)
            }));

            match res {
                Ok(Poll::Ready(())) => {}
                Ok(Poll::Pending) => {
                    task.join_state.borrow_mut().task_waker = Some(maybe_local_waker);
                    this.executor.state.tasks.borrow_mut().insert(task_id, task);
                }
                Err(_) => {
                    eprintln!("task {} panicked", task_id);
                }
            }

            if let Poll::Ready(res) = this.poll_main_fut(&main_waker, &mut main_context) {
                return Poll::Ready(res);
            }

            // Allow polling one at a time in a cooperative system
            if this.cooperative {
                if !this.woken.is_empty() {
                    cx.waker().wake_by_ref();
                }
                return Poll::Pending;
            }
        }
    }
}

impl Wake for ThreadWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref();
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.thread.unpark();
    }
}

impl Wake for MainWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.woken.store(true, std::sync::atomic::Ordering::Release);
        self.parent.wake_by_ref();
    }
}

impl Wake for MaybeLocalWaker {
    fn wake(self: Arc<Self>) {
        self.wake_by_ref()
    }

    fn wake_by_ref(self: &Arc<Self>) {
        if self.remote_state.thread_id == std::thread::current().id() {
            with_local_exec(|exec| exec.woken.borrow_mut().push_back(self.task_id));
        } else {
            self.remote_state
                .woken
                .lock()
                .unwrap()
                .push_back(self.task_id);
        }
        self.parent.wake_by_ref();
    }
}

impl Default for Executor {
    fn default() -> Self {
        Self::new()
    }
}

impl Default for RemoteState {
    fn default() -> Self {
        Self::new()
    }
}

impl Default for ThreadWaker {
    fn default() -> Self {
        Self::new()
    }
}

impl std::fmt::Display for JoinError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Task panicked")
    }
}

impl std::error::Error for JoinError {}

impl Drop for ExecutorToken {
    fn drop(&mut self) {
        LOCAL_EXECUTOR.with(|exec| exec.borrow_mut().take());
    }
}

impl<T> Drop for JoinNotifier<T> {
    fn drop(&mut self) {
        let mut guard = self.task_state.borrow_mut();
        guard.dropped = true;
        if let Some(waker) = guard.handle_waker.take() {
            waker.wake();
        }
    }
}
