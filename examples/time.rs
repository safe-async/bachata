use std::time::Duration;

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    foxtrot::block_on(bachata::run_with(async {
        println!("hewwo");

        let handles = (1..=50)
            .map(|i| {
                bachata::spawn(async move {
                    foxtrot::time::sleep(Duration::from_secs(2)).await;
                    println!("{} slept", i);
                })
            })
            .collect::<Vec<_>>();

        for handle in handles {
            handle.await?;
        }

        println!("Joined all");

        Ok(())
    }))?
}
