use std::time::Duration;

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    foxtrot::block_on(bachata::run_with(async {
        println!("hai!!!");

        let handle = bachata::spawn(async move {
            foxtrot::time::sleep(Duration::from_secs(2)).await;
            println!("slept");
        });

        handle.abort();

        println!("This will error");
        handle.await?;

        Ok(())
    }))?
}
